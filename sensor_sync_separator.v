module sensor_sync_separator(
  input CLK,
  input [13:0] DATA_IN,
  input SPI_ACTIVE,
  input wire [11:0] rb_sync_slice_fe,
  input wire [11:0] rb_sync_slice_re,
  input wire [11:0] rb_spi_slice,
  input wire rb_initiate_lock,
  input wire rb_use_internal_timing,
  // outputs
  output reg [13:0] cot_data_out = 0,
  output reg cot_valid_out = 1'b0,
  output reg [9:0] LINE = 0,
  output reg SPI_MARKER = 1'b0,
  output reg AEC_MARKER = 1'b0,
  output reg spi_period = 1'b0,
  output reg sync_6_Gb_R = 1'b0,
  output reg sync_11_B = 1'b0,
  output reg sync_14_B_blk = 1'b0,
  output reg sync_17_Gr = 1'b0,
  output reg sync_20_Gr_blk = 1'b0,
  output reg sync_30_vblank = 1'b0,
  output reg sync_92_fstart = 1'b0,
  // debug
  output reg csync_tp = 1'b0,
  output reg spi_data = 1'b0,
  output reg sample_288_tp = 1'b0,
  output reg sample_1152_tp = 1'b0,
  output reg sample_288_1152_tp = 1'b0,
  output reg sample_288_eq_816_tp = 1'b0,
  output reg sample_1152_eq_27_tp,
  output wire [11:0] rb_pulse_count_288,
  output wire [11:0] rb_pulse_count_1152,
  output reg valid_int_tp = 1'b0,
  output reg sync_92_fstart_int_tp = 1'b0,
  output reg line_count_int_0_tp = 1'b0,
  output reg valid_xor_tp = 1'b0,
  output reg sync_92_fstart_xor_tp = 1'b0,
  output reg sync_92_fstart_period_eq_40f80_tp = 1'b0,
  // debug FIFO
  input wire [9:0] rb_cot_fifo_line,
  output reg cot_fifo_en = 1'b0,
  output reg [13:0] cot_fifo_data = 0
);

// generate positive composite sync from sligins analog sync tip
reg csync = 1'b0;
reg csync_dly = 1'b0;
reg csync_re = 1'b0;
reg [2:0] csync_re_dly = 0;
reg csync_fe = 1'b0;

reg [10:0] sample = 0;
reg [10:0] sample_reg = 0;
reg [7:0] sync_length = 0;
reg [7:0] sync_length_reg = 0;

reg [13:0] data_dly1 = 0;
reg [13:0] data_dly2 = 0;
reg [13:0] data_dly3 = 0;
reg [13:0] data_dly4 = 0;
reg [13:0] data_dly5 = 0;

reg valid_line = 1'b0;

reg valid_cot = 1'b0;

reg [2:0] SPI_ACTIVE_dly = 0;

reg sync_period = 1'b0;
reg spi_period_mask = 1'b0;

reg sample_288_pulse = 1'b0;
reg sample_1152_pulse = 1'b0;

reg sync_92_fstart_cot = 1'b0;
reg [2:0] sync_92_fstart_cot_dly = 0;
reg sync_92_fstart_cot_re = 1'b0;
reg sync_92_fstart_cot_re_dly = 1'b0;

reg [19:0] sync_92_fstart_period = 0;
reg [19:0] sync_92_fstart_period_reg = 0;

reg [11:0] pulse_count_288 = 0;
reg [11:0] pulse_count_288_reg = 0;

reg [11:0] pulse_count_1152 = 0;
reg [11:0] pulse_count_1152_reg = 0;

assign rb_pulse_count_288 = pulse_count_288_reg;
assign rb_pulse_count_1152 = pulse_count_1152_reg;

reg rb_initiate_lock_reg = 1'b0;
reg rb_use_internal_timing_reg = 1'b0;  

reg disable_int_timing = 1'b0;

reg first_csync_re = 1'b0;
reg csync_re_int = 1'b0;

reg [10:0] sample_count_int = 0;
reg [9:0] line_count_int = 0;
reg last_line_int = 1'b0;

reg valid_line_int = 1'b0;
reg valid_int = 1'b0;

reg sync_period_int = 1'b0;
reg sync_92_fstart_int = 1'b0;

reg [9:0] LINE_plus1 = 0;

always @(posedge CLK) 
  begin

    SPI_ACTIVE_dly<={SPI_ACTIVE_dly[1:0],SPI_ACTIVE};

    // generate positive composite sync
    if((DATA_IN[13:2]<rb_sync_slice_fe)&&(spi_period_mask==1'b0))
      begin csync <= 1'b1; end
    else if(DATA_IN[13:2]>rb_sync_slice_re)	
      begin csync <= 1'b0; end
    csync_dly<=csync;
    csync_re<=((csync_dly==1'b0)&&(csync==1'b1)) ? 1'b1 : 1'b0;
    csync_fe<=((csync_dly==1'b1)&&(csync==1'b0)) ? 1'b1 : 1'b0;
    csync_tp<=csync_dly;

    // count the number of samples between syncs
    if(csync_re==1'b1)
      begin 
        sample<=11'd1; 
        sample_reg<=sample;
      end
    else
      begin sample<=sample+11'd1; end

    // 288 clocks/line for 816 lines
    sample_288_tp<=(sample_reg==11'd288) ? 1'b1 : 1'b0;
    // 288*4=1152 clocks/line for 27 lines during vertical blanking
    sample_1152_tp<=(sample_reg==11'd1152) ? 1'b1 : 1'b0;

    sample_288_1152_tp<=sample_288_tp|sample_1152_tp;

    // count number of clocks in each sync pulse
    if(csync_dly==1'b0)
      begin sync_length<=8'd0; end
    else
      begin sync_length<=sync_length+8'd1; end       

    // latch sync length count
    if(csync_fe==1'b1)
      begin sync_length_reg<=sync_length; end

    // generate 128 clock wide pulse within middle of 288 clock line
    sync_period<=((sample>=11'd104)&&(sample<11'd232)) ? 1'b1 : 1'b0;

    sync_6_Gb_R<=((sync_length_reg==8'd6)||(sync_length_reg==8'd7)) ? sync_period : 1'b0;
    sync_11_B<=((sync_length_reg==8'd11)||(sync_length_reg==8'd12)) ? sync_period : 1'b0;
    sync_14_B_blk<=((sync_length_reg==8'd14)||(sync_length_reg==8'd15)) ? sync_period : 1'b0;
    sync_17_Gr<=((sync_length_reg==8'd17)||(sync_length_reg==8'd18)) ? sync_period : 1'b0;
    sync_20_Gr_blk<=((sync_length_reg==8'd20)||(sync_length_reg==8'd21)) ? sync_period : 1'b0;
    sync_30_vblank<=((sync_length_reg==8'd30)||(sync_length_reg==8'd31)) ? sync_period : 1'b0;
    sync_92_fstart_cot<=((sync_length_reg==8'd92)||(sync_length_reg==8'd93)) ? sync_period : 1'b0;
        
    // line counter
    if(sync_92_fstart_cot==1'b1)
      begin LINE<=10'd0; end
    else if(csync_re==1'b1)
      begin LINE<=LINE+10'd1; end

    LINE_plus1<=LINE+10'd1;

    if(sync_92_fstart_cot==1'b1)
      begin cot_fifo_en<=1'b0; end
    else if((LINE_plus1==rb_cot_fifo_line)&&(csync_re==1'b1))
      begin cot_fifo_en<=1'b1; end
    cot_fifo_data<=data_dly5;

    spi_period_mask<=((LINE==10'd817)&&(sample>11'd47)&&(sample<11'd864)) ? 1'b1 : 1'b0;
    SPI_MARKER<=((LINE==10'd817)&&(sample>11'd96)&&(sample<11'd128)) ? 1'b1 : 1'b0;
    spi_period<=((LINE==10'd817)&&(sample>11'd63)&&(sample<11'd800)) ? 1'b1 : 1'b0;
    AEC_MARKER<=((LINE==10'd815)&&(sample>11'd31)&&(sample<11'd64)) ? 1'b1 : 1'b0;

    // delay data to match delay in valid generation
    data_dly1<=DATA_IN;
    data_dly2<=data_dly1;
    data_dly3<=data_dly2;
    data_dly4<=data_dly3;
    data_dly5<=data_dly4;

    // SPI data is above normal video
    spi_data<=(data_dly5[13:2]>rb_spi_slice) ? 1'b1 : 1'b0;

    valid_line<=((LINE>=10'd5)&&(LINE<=10'd816)) ? 1'b1 : 1'b0;

    // generate data valid
    // valid data is 77-280 = 204 pixels 
	  if ((valid_line==1'b1)&&(sample>=11'd77)&&(sample<=11'd280))
		  begin valid_cot <= 1'b1; end
	  else
		  begin valid_cot <= 1'b0; end

		cot_data_out <= data_dly4;

    csync_re_dly<={csync_re_dly[1:0],csync_re};

    // generate pulses that indicate that previous line period was 288 or 1152
    sample_288_pulse<=(csync_re_dly[2]==1'b1) ? sample_288_tp : 1'b0;
    sample_1152_pulse<=(csync_re_dly[2]==1'b1) ? sample_1152_tp : 1'b0;

    sync_92_fstart_cot_dly<={sync_92_fstart_cot_dly[1:0],sync_92_fstart_cot};
    sync_92_fstart_cot_re<=((sync_92_fstart_cot_dly[2]==1'b0)&&(sync_92_fstart_cot_dly[1]==1'b1)) ? 1'b1 : 1'b0;
    sync_92_fstart_cot_re_dly<=sync_92_fstart_cot_re;

    if((sync_92_fstart_cot_re==1'b1)||(sync_92_fstart_period[19]==1'b1))
      begin
        sync_92_fstart_period_reg<=sync_92_fstart_period;
        sync_92_fstart_period<=20'd1;
      end
    else
      begin sync_92_fstart_period<=sync_92_fstart_period+20'd1; end

    sync_92_fstart_period_eq_40f80_tp<=(sync_92_fstart_period_reg==20'h40f80) ? 1'b1 : 1'b0;

   // count the number of line periods with period of 288 clocks
    if(sync_92_fstart_cot_re==1'b1)
      begin
        pulse_count_288_reg<=pulse_count_288;
        pulse_count_288<=12'd0;
      end
    else if(sample_288_pulse==1'b1)
      begin pulse_count_288<=pulse_count_288+12'd1; end
 
   // count the number of line periods with period of 288*4 = 1152 clocks
    if(sync_92_fstart_cot_re==1'b1)
      begin
        pulse_count_1152_reg<=pulse_count_1152;
        pulse_count_1152<=12'd0;
      end
    else if(sample_1152_pulse==1'b1)
      begin pulse_count_1152<=pulse_count_1152+12'd1; end  

    // are the number of lines with each valid sample count as expected?
    sample_288_eq_816_tp<=(pulse_count_288_reg==12'd816) ? 1'b1 : 1'b0;
    sample_1152_eq_27_tp<=(pulse_count_1152_reg==12'd27) ? 1'b1 : 1'b0;

     // start internal timing at frame boundary
     if(sync_92_fstart_cot_re==1'b1)
      begin
        rb_initiate_lock_reg<=rb_initiate_lock;
        rb_use_internal_timing_reg<=rb_use_internal_timing;    
      end

    disable_int_timing<=((rb_initiate_lock_reg==1'b0)&&(rb_use_internal_timing_reg==1'b0)) ? 1'b1 : 1'b0;

    /// flag to indicate first csync rising edge after frame start
    if(sync_92_fstart_cot_re_dly==1'b1)
      begin first_csync_re<=rb_initiate_lock_reg; end
    else if(csync_re==1'b1)
      begin first_csync_re<=1'b0; end

    csync_re_int<=(sample_count_int==11'd287) ? 1'b1 : 1'b0;

    // lock internal sample count to external COT timing when commanded
    if(((first_csync_re==1'b1)&&(csync_re==1'b1))||(csync_re_int==1'b1)||(disable_int_timing==1'b1))
      begin sample_count_int<=11'd1; end
    else
      begin sample_count_int<=sample_count_int+11'd1; end

    last_line_int<=(line_count_int==10'd924) ? 1'b1 : 1'b0;

    // lock internal line count to external COT timing
    if(((first_csync_re==1'b1)&&(csync_re==1'b1))||((last_line_int==1'b1)&&(csync_re_int==1'b1))||(disable_int_timing==1'b1))
      begin line_count_int<=10'd1; end
    else if(csync_re_int==1'b1)
      begin line_count_int<=line_count_int+10'd1; end

    line_count_int_0_tp<=line_count_int[0];

    // create valid from internal timing
    valid_line_int<=((line_count_int>=10'd5)&&(line_count_int<=10'd816)) ? 1'b1 : 1'b0;

	  if ((valid_line_int==1'b1)&&(sample_count_int>=11'd77)&&(sample_count_int<=11'd280))
		  begin valid_int<=1'b1; end
	  else
		  begin valid_int<=1'b0; end

    valid_int_tp<=valid_int;

    // create sync_92_fstart from internal timing
    sync_period_int<=((sample_count_int>=11'd104)&&(sample_count_int<11'd232)) ? 1'b1 : 1'b0;
    sync_92_fstart_int<=(line_count_int==10'd921) ? sync_period_int : 1'b0;
    sync_92_fstart_int_tp<=sync_92_fstart_int;

    cot_valid_out<=(rb_use_internal_timing_reg==1'b0) ? valid_cot : valid_int;
    sync_92_fstart<=(rb_use_internal_timing_reg==1'b0) ? sync_92_fstart_cot : sync_92_fstart_int;

    valid_xor_tp<=valid_cot^valid_int;
    sync_92_fstart_xor_tp<=sync_92_fstart_cot^sync_92_fstart_int;

  end

endmodule
