// Test Bench for Adaptive Sync

// Adpative Sync Module gets deserialized data, adaptivly finds the sync value of the sensor.

`timescale 1 ps / 1 ps


module TestBench (); 

reg ref_word_clk;

reg [13:0] ImageData [0:266111];	//Image loaded from text file
reg [13:0] vid_adc = 14'd16383;
reg [19:0] count = 20'd0;
reg reset = 1'b1;

wire [11:0] sync_thresh_re;
wire [11:0] sync_thresh_fe;

wire [13:0] cot_data_out;
wire cot_valid_out;
wire [13:0] min_vid_adc;

integer vid_adc_int;
integer sync_thresh_re_int;
integer sync_thresh_fe_int;
integer count_int;
integer min_vid_adc_int;

assign sync_thresh_re_int = sync_thresh_re;
assign sync_thresh_fe_int = sync_thresh_fe;
assign count_int = count;
assign min_vid_adc_int = min_vid_adc;

reg init_lock;
reg use_internal;
reg add_offset = 1'b0;

initial begin
	$readmemh("Image.data",ImageData);
end


// Clock
initial begin
	ref_word_clk = 1'b0;
	forever #62500 ref_word_clk = ~ref_word_clk; //8MHz Clock?
end

initial begin
	reset = 1'b1;
	#125000 reset = 1'b0;
	
	init_lock = 1'b0;
	use_internal = 1'b1;
	
	#(64'd66528000000) init_lock = 1'b1;
	#(64'd66528000000) init_lock = 1'b0;
	#125000 reset = 1'b1;
	#125000 reset = 1'b0;
	// #(64'd66528000000) use_internal = 1'b1;
	#(64'd66528000000) add_offset = 1'b1;
end

always @(posedge ref_word_clk) begin
	vid_adc <= (add_offset) ? ImageData[count] + 14'd1000 : ImageData[count];
	vid_adc_int <= (add_offset) ? ImageData[count] + 14'd1000 : ImageData[count];
	count <= (count < 266111) ? count + 20'd1: 20'd0;
end

adaptive_sync #(.risespeed(12)) adaptive_sync_inst(
	//inputs
	.CLK(ref_word_clk),
	.RST(reset),
	.DATA_IN(vid_adc),
	.OFFSET_THRESH_RE(14'd200),
	.OFFSET_THRESH_FE(14'd300),
	//outputs
	.SYNC_THRESH_RE(sync_thresh_re),
	.SYNC_THRESH_FE(sync_thresh_fe),
	.MIN_DATA(min_vid_adc)
);

sensor_sync_separator sensor_sync_separator_inst(
  // inputs
  .CLK(ref_word_clk),
  .DATA_IN(vid_adc),
  .SPI_ACTIVE(1'b0),
  .rb_sync_slice_fe(sync_thresh_fe),
  .rb_sync_slice_re(sync_thresh_re),
  .rb_spi_slice(12'he00),
  .rb_initiate_lock(init_lock),
  .rb_use_internal_timing(use_internal),
  // outputs
  .cot_data_out(cot_data_out),
  // 204 active pixels by 812 lines (4 optical black and 808 active)
  .cot_valid_out(cot_valid_out), 
  .LINE(), // debug
  .SPI_MARKER(),
  .AEC_MARKER(),
  .spi_period(),
  // the following signals are asserted for pixels 104-232 when the sync length and type is as indicated
  // mainly used for debug except for sync_92_fstart which is the COT frame sync
  .sync_6_Gb_R(),
  .sync_11_B(),
  .sync_14_B_blk(),
  .sync_17_Gr(),
  .sync_20_Gr_blk(),
  .sync_30_vblank(),
  .sync_92_fstart(),
  // debug
  .csync_tp(),
  .spi_data(),
  .sample_288_tp(),
  .sample_1152_tp(),
  .sample_288_1152_tp(),
  .sample_288_eq_816_tp(),
  .sample_1152_eq_27_tp(),
  .rb_pulse_count_288(),
  .rb_pulse_count_1152(),
  .valid_int_tp(),
  .sync_92_fstart_int_tp(),
  .line_count_int_0_tp(),
  // these signals will go high when the COT and INT timing signals are not equal
  .valid_xor_tp(),
  .sync_92_fstart_xor_tp(),
  .sync_92_fstart_period_eq_40f80_tp(),
  // debug FIFO
  .rb_cot_fifo_line(),
  .cot_fifo_en(),
  .cot_fifo_data()
);


endmodule



















// Test Code from: https://stackoverflow.com/questions/628603/readmemh-writememh-related-resources
// Use format of files to create new ones and load into a register

// integer i;
// reg [7:0] memory [0:15]; // 8 bit memory with 16 entries

// initial begin
    // for (i=0; i<16; i++) begin
        // $cast(memory[i],i);
		// // memory = i;
    // end
    // $writememb("memory_binary.txt", memory);
    // $writememh("memory_hex.txt", memory);
// end