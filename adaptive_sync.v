// Adaptive Sync Module for Nano Camera
// Looks for the lowest value and saves it
// Saved value will slowly rise overtime to prevent getting stuck. 
// To speed up rise time choose a lower value for risespeed parameter. 0 = fastest.

// Saved Minimum Value will rise by 1 bit for every 2^risespeed clock cycles.

// 8MHz/30Hz = 266,667 cycles per frame. 
// When risespeed = 12, Saved minimum will rise by ~65 per frame (8,000,000/30/2^12 = 65)

module adaptive_sync #(
	parameter risespeed = 12
	)
	(
	input wire CLK,
	input wire RST,								//Reset sets min_video to 2000
	input wire [13:0] DATA_IN,
	input wire [13:0] OFFSET_THRESH_RE,			//Offset applied to SYNC_THRESH_RE
	input wire [13:0] OFFSET_THRESH_FE,			//Offset applied to SYNC_THRESH_FE
	
	output wire [11:0] SYNC_THRESH_RE,
	output wire [11:0] SYNC_THRESH_FE,
	output wire [13:0] MIN_DATA
);

reg [risespeed-1:0] zeroes = 0;

reg [(13+risespeed):0] min_video = 0; //Saves minimum value in top 14 bits

assign MIN_DATA = min_video[(13+risespeed):(0+risespeed)];

always @(posedge CLK) begin
	if(RST) begin
		min_video <= {14'd2000,zeroes};
	end else begin		
		min_video <= (DATA_IN < min_video[(13+risespeed):(0+risespeed)]) ? {DATA_IN, zeroes} : min_video + 1'b1;
	end
end

//Check clipping on output > 4095
assign SYNC_THRESH_RE = (min_video[(13+risespeed):(2+risespeed)] + OFFSET_THRESH_RE > 14'h0fff) ? 12'hfff : min_video[(13+risespeed):(2+risespeed)] + OFFSET_THRESH_RE;
assign SYNC_THRESH_FE = (min_video[(13+risespeed):(2+risespeed)] + OFFSET_THRESH_FE > 14'h0fff) ? 12'hfff : min_video[(13+risespeed):(2+risespeed)] + OFFSET_THRESH_FE;

endmodule